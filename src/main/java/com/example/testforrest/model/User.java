package com.example.testforrest.model;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Data
public class User {

    @Autowired
    private Language language;

    //Language language = new Language();

    public int sum(int a, int b){
        return a+b;
    }

    public String userSayHello(){
        return language.sayHello();
    }
}
