package com.example.testforrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestForRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestForRestApplication.class, args);
    }

}
