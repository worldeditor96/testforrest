package com.example.testforrest.repository;

import com.example.testforrest.model.Book;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@DataJpaTest
class BookRepoTest {

    @Autowired
    private BookRepo bookRepo;


    @Test
    public void findByNameAndAndAuthor() {
        assertTrue(!bookRepo.findAll().iterator().hasNext());

        Book book = bookRepo.save(new Book("sdfsdf", "sdds"));
        assertTrue(bookRepo.findByNameAndAndAuthor("sdfsdf", "sdds").get().getId()==book.getId());
    }

    @Test
    public void findAll() {
        assertTrue(!bookRepo.findAll().iterator().hasNext());

        bookRepo.save(new Book("sdfsdf1", "sdds1"));
        bookRepo.save(new Book("sdfsdf22", "sdds22"));
        bookRepo.save(new Book("sdfsdf333", "sdds333"));

        assertTrue(StreamSupport.stream(bookRepo.findAll().spliterator(), false).count()==3);
    }

    public void saveNewBook() {
        assertTrue(!bookRepo.findAll().iterator().hasNext());

        Book book = bookRepo.save(new Book("sdfsdf1", "sdds1"));
        Book book2 = bookRepo.save(new Book("sdfsdf22", "sdds22"));

        assertTrue(bookRepo.save(book).getAuthor().equals("sdds1"));
        assertFalse(bookRepo.save(book).getName().equals("sdds1"));

    }


}