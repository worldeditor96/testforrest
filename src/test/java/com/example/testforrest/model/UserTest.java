package com.example.testforrest.model;

import com.example.testforrest.TestForRestApplication;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.persistence.Basic;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@SpringBootTest(classes = TestForRestApplication.class) // поднимает наше приложение и бины, которые в нем есть
class UserTest {

    private Language language;
    private User user;

    @Autowired
    User userPro;

    @BeforeEach
    public void setuo(){
        language = Mockito.mock(Language.class);
        user = new User();
        user.setLanguage(language);
    }


    @Test
    void testForsum() {
        User user = new User();
        assertEquals(10, user.sum(7, 3));
        assertTrue((user.sum(2,3))==5, "if 2+3 = 5");
        assertFalse((user.sum(22,31))==5);
    }

    @Test
    void testUserSayHello() {
        when(language.sayHello()).thenReturn("Sniper");
        user.userSayHello();
        verify(language, times(1)).sayHello();

    }

    @Test
    public void sayHello1(){
        assertTrue(userPro.userSayHello().equals("hello"));
    }

}